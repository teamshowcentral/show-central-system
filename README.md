# Show Central System

### What is this repository for? ###

* Place for Show Central's Minecraft Server System

### Contribution guidelines ###

* Unless you are an active developer working for the Greenville Project you are not allowed to clone, fork, edit, etc!

### Who do I talk to? ###

#### Viper (Head-Developer) ####
* Discord: Viper#3408
* Email: kitt3120@gmail.com
#### ZoomBug (Project-Manager / Show Central Server Owner) ####
* Discord: ZoomBug#3101
