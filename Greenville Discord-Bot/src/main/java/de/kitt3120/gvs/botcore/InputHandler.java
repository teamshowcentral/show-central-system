package de.kitt3120.gvs.botcore;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.logging.Logger;
import de.kitt3120.gvs.misc.Values;
import de.kitt3120.gvs.modules.Module;
import de.kitt3120.gvs.users.User;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class InputHandler extends ListenerAdapter implements CommandExecutor {
	
	public InputHandler() {
		Logger.startSection("Input-Handler");
		
		Logger.log("Hooking into Minecraft command system...", false);
		if(Core.getInstance().getDescription().getCommands() != null) {
			for(String commandName : Core.getInstance().getDescription().getCommands().keySet()) {
				Core.getInstance().getCommand(commandName).setExecutor(this);
				Logger.log("Hooked into Minecraft command: " + commandName + "!");
			}
		}

		Logger.log("Hooking into Discord command system...", false);
		Bot.getBot().addEventListener(this);
		Logger.endSection();
	}
	
	public void unregister() {
		Logger.startSection("Input-Handler");
		Logger.log("Unregistering");
		Bot.getBot().removeEventListener(this);
		Logger.endSection();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		//Catch ChoiceMessages
		if(command.getName().equals("TriggerChoiceMessage")) {
			new BukkitRunnable() {
				
				@Override
				public void run() {
					Core.getInstance().getModuleManager().getModule("TriggerChoiceMessage", true, false).onFireFromMinecraft(sender, args);
				}
			}.runTask(Core.getInstance());
			return true;
		}
		
		Module module = Core.getInstance().getModuleManager().getModule(command.getName(), false, false);
		if(module != null) {
			new BukkitRunnable() {
				
				@Override
				public void run() {
					if(!module.isAllowed(sender)) {
						sender.sendMessage(Module.getPrefix() + " Sorry, you are not allowed to do that!");
						return;
					}
					
					Logger.startSection("Input-Handler");
					Logger.log("User " + sender.getName() + " fired module " + module.getName() + " from Minecraft", false);
					try {
						module.onFireFromMinecraft(sender, args);
					} catch (Exception e) {
						e.printStackTrace();
						Logger.severe("Unhandled Exception: " + e.getMessage());
					}
					Logger.endSection();
				}
			}.runTask(Core.getInstance());
			return true;
		}
		return false;
	}
	
	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		if(event.getAuthor().isBot() || event.getAuthor().equals(Bot.getBot().getSelfUser())) return;
		if(event.getChannelType().equals(ChannelType.TEXT)) {
			if(!event.getGuild().getId().equals(Values.GUILD_ID)) {
				event.getChannel().sendMessage("Sorry, I am only working for " + Values.GUILD_NAME + ":wink:").queue();
				return;
			}
		} else if(!event.getChannelType().equals(ChannelType.PRIVATE)) return;
		
		if(event.getMessage().getContentDisplay().startsWith(Values.COMMAND_SYMBOL)) {
			try {
				String message = event.getMessage().getContentDisplay().substring(1);
				boolean hasArguments = message.contains(" ");
				String moduleName = hasArguments ? message.split(" ")[0] : message;
				final String[] args = hasArguments ? message.replaceFirst(moduleName, "").trim().split(" ") : new String[0];
				
				Module module = Core.getInstance().getModuleManager().getModule(moduleName);
				
				new BukkitRunnable() {
					
					@Override
					public void run() {
						if(module != null) {
							User user = Core.getInstance().getUserManager().getUser(event.getAuthor());
							if(module.isDiscordNeeded() && user == null) {
								event.getChannel().sendMessage(event.getAuthor().getAsMention() + " sorry but you have to link your Minecraft and Discord account first to be able to use this module! (Type $Link)").queue();
								return;
							}
							
							Logger.startSection("Input-Handler");
							Logger.log("User " + event.getAuthor().getName() + " fired module " + module.getName() + " from Discord", false);
							try {
								module.onFireFromDiscord(event.getAuthor(), event.getChannel(), args);
							} catch (Exception e) {
								e.printStackTrace();
								Logger.severe("Unhandled Exception: " + e.getMessage());
							}
						} else {
							event.getChannel().sendMessage("Sorry! No module named " + moduleName + " found :frowning:").queue();
						}
						Logger.endSection();
					}
				}.runTask(Core.getInstance());
			} catch (Exception e) {
				e.printStackTrace();
				Logger.startSection("Input-Handler");
				Logger.severe("Failed to handle input from Discord!");
				Logger.endSection();
			}
			
		}
	}
}
