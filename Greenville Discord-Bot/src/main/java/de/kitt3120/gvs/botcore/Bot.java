package de.kitt3120.gvs.botcore;

import javax.security.auth.login.LoginException;

import de.kitt3120.gvs.logging.Logger;
import de.kitt3120.gvs.misc.Secrets;
import de.kitt3120.gvs.misc.Values;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Guild;

public class Bot {
	
	private static JDA bot;
	
	public static void setup() {
		Logger.startSection("JDA-Bot");
		try {
			Logger.log("Creating JDA...");
			bot = new JDABuilder(AccountType.BOT).setToken(Secrets.DISCORD_SECRET).buildBlocking();
			Logger.log("Done creating JDA!");
		} catch (LoginException | InterruptedException e) {
			e.printStackTrace();
			Logger.severe("Error creating JDA!");
		}
		Logger.endSection();
	}
	
	public static JDA getBot() {
		if(bot == null) setup();
		return bot;
	}
	
	public static Guild getMainGuild() {
		return getBot().getGuildById(Values.GUILD_GREENVILLE_ID);
	}

	public static void shutdown() {
		if(bot != null) {
			bot.shutdown();
			bot = null;
		}
	}

}
