package de.kitt3120.gvs.logging;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.managers.files.FileManager;

public class Logger {
	
	private static boolean isSetUp = false;
	private static File logFile;
	private static BufferedWriter writer;
	private static ArrayList<String> sections;
	
	public static void setup() {
		if(!isSetUp) {
			try {
				logFile = FileManager.getFile("/logs/" + getCurrentLoggingTime() + ".log");
				writer = new BufferedWriter(new FileWriter(logFile, true));
				sections = new ArrayList<>();
				isSetUp = true;
			} catch (IOException e) {
				e.printStackTrace();
				Core.getInstance().getLogger().severe("Failed to setup logger!");
			}
		}
	}
	
	public static void log(String message) {
		log(message, true);
	}

	public static void log(String message, String logType) {
		log(message, logType, true);
	}
	
	public static void severe(String message) {
		severe(message, true);
	}
	
	public static void log(String message, boolean consoleOutput) {
		if(!isSetUp) setup();
		
		if(consoleOutput) Core.getInstance().getLogger().info("[" + getCurrentSection() + "] " + message);
		try {
			writer.write(getCurrentLoggingTime() + " - [INFO] [" + getCurrentSection() + "] - " + message);
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
			Core.getInstance().getLogger().severe("Failed to write to logfile!");
		}
	}
	
	public static void log(String message, String logType, boolean consoleOutput) {
		if(!isSetUp) setup();
		
		if(consoleOutput) Core.getInstance().getLogger().info("[" + getCurrentSection() + "] " + message);
		try {
			writer.write(getCurrentLoggingTime() + " - [" + logType.toUpperCase() + "] [" + getCurrentSection() + "] - " + message);
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
			Core.getInstance().getLogger().severe("Failed to write to logfile!");
		}
	}
	
	public static void severe(String message, boolean consoleOutput) {
		if(!isSetUp) setup();
		
		if(consoleOutput) Core.getInstance().getLogger().severe("[" + getCurrentSection() + "] " + message);
		try {
			writer.write(getCurrentLoggingTime() + " - [SEVERE] [" + getCurrentSection() + "] - " + message);
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
			Core.getInstance().getLogger().severe("Failed to write to logfile!");
		}
	}
	
	public static void startSection(String name) {
		if(!isSetUp) setup();
		
		sections.add(0, name);
		log("Start of logger section: " + name, false);
	}
	
	public static void endSection() {
		if(!isSetUp) setup();

		log("End of logger section: " + getCurrentSection(), false);
		try {
			sections.remove(0);
		} catch (Exception e) {
			severe("Could not end section!");
		}
	}
	
	public static String getCurrentSection() {
		if(!isSetUp) setup();
		
		if(sections.size() > 0) {
			return sections.get(0);
		}
		else return "-";
	}
	
	private static String getCurrentLoggingTime() {
		return new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(Calendar.getInstance().getTime());
	}
	
}
