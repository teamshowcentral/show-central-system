package de.kitt3120.gvs.users;

import java.util.ArrayList;

import org.bukkit.OfflinePlayer;

import de.kitt3120.gvs.Core;

public class UserParser {
	
	@SuppressWarnings("deprecation")
	public static ArrayList<User> parseUsersSafe(String line, String split) {
		ArrayList<User> users = new ArrayList<>();
		for (String name : line.split(split)) {
			users.add(Core.getInstance().getUserManager().getUser(Core.getInstance().getServer().getOfflinePlayer(name)));
		}
		return users;
	}
	
	@SuppressWarnings("deprecation")
	public static ArrayList<User> parseUsers(String line, String split) {
		ArrayList<User> users = new ArrayList<>();
		for (String name : line.split(split)) {
			OfflinePlayer p = Core.getInstance().getServer().getOfflinePlayer(name);
			if(!Core.getInstance().getUserManager().existsUser(p)) continue;
			users.add(Core.getInstance().getUserManager().getUser(p));
		}
		return users;
	}
	
	@SuppressWarnings("deprecation")
	public static User parseUserSafe(String name) {
		return Core.getInstance().getUserManager().getUser(Core.getInstance().getServer().getOfflinePlayer(name));
	}
	
	@SuppressWarnings("deprecation")
	public static User parseUser(String name) {
		OfflinePlayer p = Core.getInstance().getServer().getOfflinePlayer(name);
		if(!Core.getInstance().getUserManager().existsUser(p)) return null;
		return Core.getInstance().getUserManager().getUser(p);
	}

}
