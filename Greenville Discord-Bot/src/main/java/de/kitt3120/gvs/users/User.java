package de.kitt3120.gvs.users;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.botcore.Bot;
import de.kitt3120.gvs.logging.Logger;
import de.kitt3120.gvs.managers.files.FileManager;

public class User implements Serializable {
	
	private static final long serialVersionUID = -1542564049003547486L;
	
	private UUID userId;
	private HashMap<String, Object> properties = new HashMap<>();
	
	public User() {
		this(UUID.randomUUID());
	}
	
	public User(UUID userId) {
		this.userId = userId;
	}
	
	public Object getProperty(UserProperty property) {
		return getProperty(property.name().toLowerCase());
	}
	
	public Object getProperty(String key) {
		return properties.get(key);
	}

	public void setProperty(UserProperty property, Object value) {
		properties.put(property.name().toLowerCase(), value);
	}

	public void setProperty(String key, Object value) {
		properties.put(key, value);
	}
	
	public void removeProperty(String key) {
		properties.remove(key);
	}
	
	public void removeProperty(UserProperty property) {
		properties.remove(property.name().toLowerCase());
	}
	
	public boolean hasProperty(UserProperty property) {
		return properties.containsKey(property.name().toLowerCase());
	}
	
	public boolean hasProperty(String key) {
		return properties.containsKey(key);
	}
	
	public boolean isMinecraftOnline() {
		if(!isMinecraftBound()) return false;
		
		return Core.getInstance().getServer().getOnlinePlayers().stream().anyMatch(player -> player.getUniqueId().toString().equals(((UUID) getProperty(UserProperty.UUID_Minecraft)).toString()));
	}
	
	public boolean isMinecraftBound() {
		return hasProperty(UserProperty.UUID_Minecraft);
	}
	
	public boolean isDiscordBound() {
		return hasProperty(UserProperty.UUID_Discord);
	}
	
	public Player toMinecraftPlayer() {
		Logger.startSection("User");
		if(!isMinecraftBound()) {
			Logger.severe("Tried to get user " + userId.toString() + " but user has not yet bound a Minecraft account");
			return null;
		}
		if(!isMinecraftOnline()) {
			Logger.severe("Tried to get user " + userId.toString() + " but user is currently not online in Minecraft");
			return null;
		}
		Logger.endSection();
		
		return Core.getInstance().getServer().getPlayer((UUID) getProperty(UserProperty.UUID_Minecraft));
	}
	
	public OfflinePlayer toMinecraftPlayerOffline() {
		Logger.startSection("User");
		if(!isMinecraftBound()) {
			Logger.severe("Tried to get user " + userId.toString() + " but user has not yet bound a Minecraft account");
			return null;
		}
		Logger.endSection();
		
		return Core.getInstance().getServer().getOfflinePlayer((UUID) getProperty(UserProperty.UUID_Minecraft));
	}
	
	public net.dv8tion.jda.core.entities.User toDiscordUser() {
		Logger.startSection("User");
		if(!isDiscordBound()) {
			Logger.severe("Tried to get user " + userId.toString() + " but user has not yet bound a Discord account");
			return null;
		}
		Logger.endSection();
		
		return Bot.getBot().getUserById((String) getProperty(UserProperty.UUID_Discord));
	}
	
	public File getDataFolder() {
		return FileManager.getFile("UserData/" + userId.toString());
	}
	
	@SuppressWarnings("unchecked")
	public void loadProperties() {
		File userPropertiesFile = FileManager.getFile("UserData/" + userId.toString() + "/properties.gv");
		Logger.startSection("User");
		Logger.log("Loading properties of user " + userId.toString(), false);
		
		if(userPropertiesFile.exists()) {
			Logger.log("Found file: " + userPropertiesFile.getPath(), false);
			try {
				ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(userPropertiesFile)));
				properties = (HashMap<String, Object>) ois.readObject();
				ois.close();
				Logger.log("Loaded file: " + userPropertiesFile.getPath(), false);
			} catch (IOException e) {
				e.printStackTrace();
				Logger.severe("Could not load user's properties-map: " + userPropertiesFile.getPath());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				Logger.severe("Could not find class while reading user's propertie-map: " + userPropertiesFile.getPath());
			}
		} else {
			Logger.log("No file found! Creating empty properties map.", false);
			properties = new HashMap<>();
		}
		
		Logger.endSection();
	}
	
	public void saveProperties() {
		Logger.startSection("User");
		Logger.log("Saving properties of user " + userId.toString(), false);
		File userPropertiesFile = FileManager.getFile("UserData/" + userId.toString() + "/properties.gv");
		try {
			ObjectOutput ous = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(userPropertiesFile)));
			ous.writeObject(properties);
			ous.close();
		} catch (IOException e) {
			e.printStackTrace();
			Logger.severe("Could not save user's properties-map");
		}
		Logger.endSection();
	}

	public UUID getUserId() {
		return userId;
	}
}
