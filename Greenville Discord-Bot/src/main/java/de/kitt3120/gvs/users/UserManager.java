package de.kitt3120.gvs.users;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.logging.Logger;
import de.kitt3120.gvs.managers.files.FileManager;

public class UserManager implements Listener {
	
	private File usersFolder;
	private ArrayList<User> users;
	
	private BukkitTask autoSaver;
	
	public UserManager() {
		Logger.startSection("User-Manager");
		
		Logger.log("Initializing User-Manager", false);
		usersFolder = FileManager.getFile("UserData");
		if(usersFolder.mkdirs()) Logger.log("Created User-Data directory", false);
		
		Logger.log("Registering Listener...", false);
		Core.getInstance().getServer().getPluginManager().registerEvents(this, Core.getInstance());
		
		Logger.log("Loading all User-Datas");
		loadUsers();
		
		Logger.log("Checking online users for new users", false);
		for (Player player : Core.getInstance().getServer().getOnlinePlayers()) {
			if(!existsUser(player)) {
				Logger.log("Detected new player on start", false);
				createUserFromPlayer(player);
			}
		}

		Logger.log("Starting Auto-Saver", false);
		startAutoSaver();
		
		Logger.endSection();
	}
	
	public void loadUsers() {
		Logger.startSection("User-Manager");
		
		Logger.log("Loading Users...");
		users = new ArrayList<>();
		
		for (File userFile : usersFolder.listFiles()) {
			if(userFile.isDirectory()) {
				Logger.log("Found directory: " + userFile.getName() + ", trying to load...", false);
				User user = new User(UUID.fromString(userFile.getName()));
				users.add(user);
				user.loadProperties();
				Logger.log("Loaded user " + user.getUserId().toString(), false);
			}
		}
		
		Logger.log("Loaded " + users.size() + " user-data files");
		Logger.endSection();
	}
	
	public void saveUsers() {
		Logger.startSection("User-Manager");
		Logger.log("Saving all users", false);
		for (User user : users) {
			user.saveProperties();
		}
		Logger.endSection();
	}
	
	public void registerUser(User user) {
		if(!users.contains(user)) {
			Logger.startSection("User-Manager");
			Logger.log("Registering new user! - " + user.getUserId().toString(), false);
			users.add(user);
			Logger.endSection();
		}
	}
	
	public void createUserFromPlayer(Player player) {
		Logger.startSection("User-Manager");
		Logger.log("Creating new user: " + player.getName(), false);
		User user = new User();
		user.setProperty(UserProperty.UUID_Minecraft, player.getUniqueId());
		user.saveProperties();
		registerUser(user);
		Logger.endSection();
	}
	
	public boolean existsUser(Player player) {
		return getUser(player) != null;
	}
	
	public boolean existsUser(OfflinePlayer offlinePlayer) {
		return getUser(offlinePlayer) != null;
	}
	
	public boolean existsUser(String minecraftUserName) {
		return getUser(minecraftUserName) != null;
	}
	
	public boolean existsUser(net.dv8tion.jda.core.entities.User discordUser) {
		return getUser(discordUser) != null;
	}
	
	public boolean existsUser(UUID userId) {
		return getUser(userId) != null;
	}
	
	public User getUser(Player player) {
		for (User user : users) {
			if(user.isMinecraftBound() && user.isMinecraftOnline() && user.toMinecraftPlayer().equals(player)) return user;
		}
		return null;
	}
	
	public User getUser(OfflinePlayer offlinePlayer) {
		for (User user : users) {
			if(user.isMinecraftBound() && user.toMinecraftPlayerOffline().getUniqueId().toString().equals(offlinePlayer.getUniqueId().toString())) return user;
		}
		return null;
	}

	public User getUser(String minecraftName) {
		for (User user : users) {
			if(user.isMinecraftBound() && user.toMinecraftPlayerOffline().getName().equals(minecraftName)) return user;
		}
		return null;
	}
	
	public User getUser(net.dv8tion.jda.core.entities.User discordUser) {
		for (User user : users) {
			if(user.isDiscordBound() && user.toDiscordUser().getId().equals(discordUser.getId())) return user;
		}
		return null;
	}

	public User getUser(UUID userId) {
		for (User user : users) {
			if(user.getUserId().toString().equals(userId.toString())) return user;
		}
		return null;
	}
	
	public void startAutoSaver() {
		if(autoSaver == null) {
			Logger.startSection("User-Manager Auto-Saver");
			Logger.log("Starting Auto-Saver", false);
			autoSaver = new BukkitRunnable() {
				
				@Override
				public void run() {
					saveUsers();
				}
			}.runTaskTimerAsynchronously(Core.getInstance(), 20 * 60L, 20 * 60L);
			Logger.endSection();
		}
	}

	public void stopAutoSaver() {
		if(autoSaver != null) {
			Logger.startSection("User-Manager Auto-Saver");
			Logger.log("Stopping Auto-Saver", false);
			autoSaver.cancel();
			autoSaver = null;
			Logger.endSection();
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		if(!existsUser(e.getPlayer())) {
			Logger.startSection("User-Manager");
			Logger.log("Detected new player on join", false);
			createUserFromPlayer(e.getPlayer());
			Logger.endSection();
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		if(existsUser(e.getPlayer())) {
			Logger.startSection("User-Manager");
			Logger.log("Saving player on quit: " + e.getPlayer().getName(), false);
			getUser(e.getPlayer()).saveProperties();
			Logger.endSection();
		}
	}

	public ArrayList<User> getUsers() {
		return users;
	}
	
}
