package de.kitt3120.gvs.misc;

import de.kitt3120.gvs.users.User;

public class Permissions {
	//Staff
	public static final String Admin = "greenville.admin";
	
	//Systems (Including modules)
	public static final String Mailing = "greenville.mailing";
	
	//Single modules
	public static final String Module_Link = "greenville.module.link";
	
	public static boolean isAdmin(User user) {
		return user.toMinecraftPlayerOffline().isOp() || user.toMinecraftPlayer().hasPermission(Admin);
	}

}
