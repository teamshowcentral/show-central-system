package de.kitt3120.gvs.interaction.discord.polls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.botcore.Bot;
import de.kitt3120.gvs.logging.Logger;
import de.kitt3120.gvs.users.User;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveEvent;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class Poll extends ListenerAdapter {
	
	private Poll instance;
	
	private User author;
	private MessageChannel channel;
	private Message message;
	private String messageString;
	private int timeout;
	private boolean hasTimeout;
	private long refreshRate;
	
	private String[] options;
	
	private boolean isRunning = false;
	
	private BukkitTask finishChecker;
	private ArrayList<PollListener> pollListeners = new ArrayList<>();
	
	private HashMap<String, Integer> votes = new HashMap<>();
	
	public Poll(User author, MessageChannel channel, String message) {
		this(author, channel, message, 0, true);
	}
	
	public Poll(User author, MessageChannel channel, String message, int timeout) {
		this(author, channel, message, timeout, true);
	}
	
	public Poll(User author, MessageChannel channel, String message, int timeout, boolean autoStart) {
		instance = this;
		
		this.author = author;
		this.channel = channel;
		this.messageString = message;
		this.timeout = timeout;
		this.hasTimeout = timeout > 0;
		this.refreshRate = 20;
		
		this.options = new String[] {"white_check_mark", "x"}; //String-Array of emojis representing the options
		
		if(autoStart) start();
	}
	
	public void start() {
		if(!isRunning) {
			Bot.getBot().addEventListener(this);
			try {
				this.message = channel.sendMessage(getMessageDisplay()).complete(true);
				for (String option : getOptions()) {
					Emoji e = EmojiManager.getForAlias(option);
					if(e != null) message.addReaction(e.getUnicode()).queue();
				}
			} catch (RateLimitedException e) {
				e.printStackTrace();
				Logger.startSection("Poll");
				Logger.severe("Failed to start because of ratelimit!");
				Logger.endSection();
			}
			
			finishChecker = new BukkitRunnable() {
				
				@Override
				public void run() {
					if(isFinished()) {
						new BukkitRunnable() {
							
							@Override
							public void run() {
								stop(); //Run in another task so if the finishChecker gets cancelled in the stop() method it wont cancel the stop() method itself!
							}
						}.runTask(Core.getInstance());
						if(isRunning) cancel();
					} else {
						if(hasTimeout() && timeout > 0) timeout--;
						try {
							message.editMessage(getMessageDisplay()).queue();
							message = channel.getMessageById(message.getId()).complete(true);
							updateVotes();
						} catch (RateLimitedException e) {}
						pollListeners.forEach(listener -> listener.onTick(instance));
					}
				}
			}.runTaskTimerAsynchronously(Core.getInstance(), refreshRate, refreshRate);
			
			pollListeners.forEach(listener -> listener.onStart(instance));
			isRunning = true;
		}
	}
	
	public void stop() {
		stop(false);
	}
	
	public void stop(boolean ignoreListeners) {
		if(isRunning) {
			updateVotes();
			
			finishChecker.cancel();
			
			if(!ignoreListeners) pollListeners.forEach(listener -> listener.onTick(instance));
			if(!ignoreListeners) pollListeners.forEach(listener -> listener.onStop(instance));
			Bot.getBot().removeEventListener(this);
			isRunning = false;
		}
	}
	
	public String getMessageDisplay() {
		return "```" + messageString + "\n" + (hasTimeout ? "Time left: " + timeout + " " + (refreshRate == 20 ? "seconds" : "ticks") + "```": "```");
	}
	
	public void updateVotes() {
		votes.clear();
		List<net.dv8tion.jda.core.entities.User> voted = new ArrayList<>();
		
		message.getReactions().forEach(reaction -> {
			String name = EmojiManager.getByUnicode(reaction.getReactionEmote().getName()).getAliases().get(0);
			
			boolean isValid = false;
			for(String option : getOptions()) if(option.equalsIgnoreCase(name)) isValid = true;
			
			if(isValid) {
				int count = 0;
				try {
					for (net.dv8tion.jda.core.entities.User voter : reaction.getUsers().complete(true)) {
						if(!voted.contains(voter) && !voter.getId().equals(Bot.getBot().getSelfUser().getId())) {
							voted.add(voter);
							count ++;
						}
					}
				} catch (RateLimitedException e) {}
				votes.put(name, count);
			}
		});
	}
	
	public boolean isFinished() {
		return hasTimeout && timeout <= 0;
	}
	
	public void addPollListener(PollListener pollListener) {
		if(!pollListeners.contains(pollListener)) pollListeners.add(pollListener);
	}
	
	public void removePollListener(PollListener pollListener) {
		if(pollListeners.contains(pollListener)) pollListeners.remove(pollListener);
	}

	public ArrayList<PollListener> getPollListeners() {
		return pollListeners;
	}
	
	public boolean hasAuthor() {
		return author != null;
	}
	
	public boolean hasOptions() {
		return getOptions().length > 0;
	}
	
	public boolean hasTimeout() {
		return hasTimeout;
	}
	
	public boolean isRunning() {
		return isRunning;
	}

	public User getAuthor() {
		return author;
	}

	public MessageChannel getChannel() {
		return channel;
	}

	public Message getMessage() {
		return message;
	}

	public String[] getOptions() {
		return options;
	}

	public void setOptions(String[] options) {
		this.options = options;
	}

	public int getTimeout() {
		return timeout;
	}
	
	public HashMap<String, Integer> getVotes() {
		return votes;
	}
	
	public int getVotesCount() {
		int i = 0;
		for (int count : votes.values()) {
			i += count;
		}
		return i;
	}
	
	public int getVotes(String optionName) {
		if(votes.containsKey(optionName.toLowerCase())) return votes.get(optionName.toLowerCase());
		return 0;
	}
	
	public int getVotes(int optionId) {
		if(getOptions().length > optionId && votes.containsKey(getOptions()[optionId].toLowerCase())) return votes.get(getOptions()[optionId].toLowerCase());
		return 0;
	}

	public BukkitTask getFinishChecker() {
		return finishChecker;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	
	public void setRefreshRate(long refreshRate) {
		this.refreshRate = refreshRate;
	}
	
	public long getRefreshRate() {
		return refreshRate;
	}
	
	@Override
	public void onMessageReactionAdd(MessageReactionAddEvent event) {
		if(isRunning) pollListeners.forEach(listener -> listener.onReactionAdded(instance, event));
	}
	
	@Override
	public void onMessageReactionRemove(MessageReactionRemoveEvent event) {
		if(isRunning) pollListeners.forEach(listener -> listener.onReactionRemoved(instance, event));
	}

	public void setMessage(String message) {
		this.messageString = message;
	}

}
