package de.kitt3120.gvs.interaction.minecraft.choicemessages;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.logging.Logger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

public class ChoiceMessage {
	
	private UUID uuid;
	
	private String message;
	private ChatColor mainColor;
	private ArrayList<Choice> choices;
	
	private int timeout;
	private boolean hasTimeout;
	private BukkitTask timeoutTask;
	
	private boolean isActive = false;
	private boolean killOnChoice = true;
	
	private ArrayList<ChoiceMessageListener> choiceMessageListeners;

	public ChoiceMessage(String message) {
		this(message, 0, true);
	}
	
	public ChoiceMessage(String message, int timeout) {
		this(message, timeout, true);
	}
	
	public ChoiceMessage(String message, int timeout, boolean autoRegister) {
		uuid = UUID.randomUUID();
		choices = new ArrayList<>();
		choiceMessageListeners = new ArrayList<>();
		
		this.message = message;
		this.mainColor = ChatColor.GRAY;
		
		this.timeout = timeout;
		this.hasTimeout = timeout > 0;
		
		if(autoRegister) register();
	}
	
	public void register() {
		if(!isActive) {
			ChoiceMessageRegistry.register(this);
			if(hasTimeout) {
				timeoutTask = new BukkitRunnable() {
					
					@Override
					public void run() {
						new BukkitRunnable() {
							
							@Override
							public void run() {
								choiceMessageListeners.stream().forEach(listener -> listener.onTimeout(getInstance()));
								kill(); //Run in another task so if the timeoutTask gets cancelled in the kill() method it wont cancel the kill() method itself!
							}
						}.runTask(Core.getInstance());
					}
				}.runTaskLater(Core.getInstance(), timeout * 20L);
			}
			
			choiceMessageListeners.stream().forEach(listener -> listener.onRegister(this));
			isActive = true;
		}
	}
	
	public void kill() {
		if(isActive) {
			if(timeoutTask != null) timeoutTask.cancel();
			ChoiceMessageRegistry.unregister(this);
			isActive = false;
			choiceMessageListeners.forEach(listener -> listener.onKill(this));
		}
	}
	
	public void choice(Player player, int id) {
		if(isActive) {
			if(choices.size() > id) {
				Choice choice = choices.get(id);
				
				new BukkitRunnable() {
					
					@Override
					public void run() {
						choice.getAction().run();
					}
				}.runTask(Core.getInstance());
				
				choiceMessageListeners.forEach(listener -> listener.onChoice(this, choice, id));
				
				if(killOnChoice) kill();
			} else {
				Logger.startSection("ChoiceMessage");
				Logger.severe("Player " + player.getName() + " made an invalid choice, id " + id + " out of " + (choices.size() - 1));
				Logger.endSection();
			}
		}
	}
	
	public void send(Player player) {
		if(isActive) {
			TextComponent msg = new TextComponent();
			msg.setColor(mainColor);
			msg.addExtra(message);
			msg.addExtra("\n");
			int choiceId = 0;
			for (Choice choice : choices) {
				TextComponent choiceComponent = new TextComponent(choice.getText());
				choiceComponent.setColor(choice.getColor());
				choiceComponent.setBold(choice.isBold());
				choiceComponent.setItalic(choice.isItalic());
				choiceComponent.setUnderlined(choice.isUnderlined());
				choiceComponent.setStrikethrough(choice.isStrikethrough());
				choiceComponent.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/TriggerChoiceMessage " + uuid.toString() + " " + choiceId));
				msg.addExtra("[ ");
				msg.addExtra(choiceComponent);
				msg.addExtra(" ] ");
				
				choiceId++;
			}
			
			player.spigot().sendMessage(msg);
		} else {
			Logger.startSection("ChoiceMessage");
			Logger.severe("Tried to send inactive ChoiceMessage to player " + player.getName());
			Logger.endSection();
		}
	}

	public void addChoiceMessageListener(ChoiceMessageListener listener) {
		if(!choiceMessageListeners.contains(listener)) choiceMessageListeners.add(listener);
	}
	
	public void removeChoiceMessageListener(ChoiceMessageListener listener) {
		if(choiceMessageListeners.contains(listener)) choiceMessageListeners.remove(listener);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setMainColor(ChatColor mainColor) {
		this.mainColor = mainColor;
	}

	public boolean isKillOnChoice() {
		return killOnChoice;
	}

	public void setKillOnChoice(boolean killOnChoice) {
		this.killOnChoice = killOnChoice;
	}

	public ArrayList<Choice> getChoices() {
		return choices;
	}
	
	public void addChoice(Choice choice) {
		if(!choices.contains(choice)) choices.add(choice);
	}
	
	public void removeChoice(Choice choice) {
		if(choices.contains(choice)) choices.remove(choice);
	}
	
	public ArrayList<ChoiceMessageListener> getChoiceMessageListeners() {
		return choiceMessageListeners;
	}

	public UUID getUuid() {
		return uuid;
	}

	public boolean isActive() {
		return isActive;
	}
	
	public boolean hasTimeout() {
		return hasTimeout;
	}
	
	private ChoiceMessage getInstance() {
		return this;
	}

}
