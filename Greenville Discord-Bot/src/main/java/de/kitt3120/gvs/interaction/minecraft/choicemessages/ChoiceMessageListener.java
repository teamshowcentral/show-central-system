package de.kitt3120.gvs.interaction.minecraft.choicemessages;

public interface ChoiceMessageListener {

	public void onChoice(ChoiceMessage choiceMessage, Choice choice, int choiceId);
	public void onRegister(ChoiceMessage choiceMessage);
	public void onKill(ChoiceMessage choiceMessage);
	public void onTimeout(ChoiceMessage choiceMessage);
	
}
