package de.kitt3120.gvs.interaction.minecraft.choicemessages;

import java.util.ArrayList;
import java.util.UUID;

public class ChoiceMessageRegistry {
	
	public static ArrayList<ChoiceMessage> choiceMessageRegister = new ArrayList<>();

	public static boolean existsMessage(UUID uuid) {
		return choiceMessageRegister.stream().anyMatch(message -> message.getUuid().equals(uuid));
	}
	
	public static boolean existsMessage(ChoiceMessage choiceMessage) {
		return choiceMessageRegister.stream().anyMatch(message -> message.equals(choiceMessage));
	}
	
	public static ChoiceMessage getChoiceMessage(UUID uuid) {
		for (ChoiceMessage choiceMessage : choiceMessageRegister) {
			if(choiceMessage.getUuid().toString().equals(uuid.toString())) return choiceMessage;
		}
		return null;
	}

	public static void register(ChoiceMessage choiceMessage) {
		if(!choiceMessageRegister.contains(choiceMessage)) choiceMessageRegister.add(choiceMessage);
	}

	public static void unregister(ChoiceMessage choiceMessage) {
		if(choiceMessageRegister.contains(choiceMessage)) choiceMessageRegister.remove(choiceMessage);
	}

}
