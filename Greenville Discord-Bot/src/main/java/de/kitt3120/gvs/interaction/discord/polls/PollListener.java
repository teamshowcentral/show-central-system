package de.kitt3120.gvs.interaction.discord.polls;

import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveEvent;

public interface PollListener {
	
	public void onTick(Poll poll);
	public void onStart(Poll poll);
	public void onStop(Poll poll);
	public void onReactionAdded(Poll poll, MessageReactionAddEvent event);
	public void onReactionRemoved(Poll poll, MessageReactionRemoveEvent event);

}
