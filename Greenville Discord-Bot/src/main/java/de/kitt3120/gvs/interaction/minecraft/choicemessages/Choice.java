package de.kitt3120.gvs.interaction.minecraft.choicemessages;

import net.md_5.bungee.api.ChatColor;

public class Choice {
	
	private String text;
	private Runnable action;
	private ChatColor color;
	private boolean bold;
	private boolean italic;
	private boolean underlined;
	private boolean strikethrough;

	public Choice(String text, ChatColor color, Runnable action) {
		this(text, color, action, false, false, false, false);
	}
	
	public Choice(String text, ChatColor color, Runnable action, boolean bold) {
		this(text, color, action, bold, false, false, false);
	}
	
	public Choice(String text, ChatColor color, Runnable action, boolean bold, boolean italic) {
		this(text, color, action, bold, italic, false, false);
	}
	
	public Choice(String text, ChatColor color, Runnable action, boolean bold, boolean italic, boolean underlined) {
		this(text, color, action, bold, italic, underlined, false);
	}
	
	public Choice(String text, ChatColor color, Runnable action, boolean bold, boolean italic, boolean underlined, boolean strikethrough) {
		this.text = text;
		this.color = color;
		this.action = action;
		this.bold = bold;
		this.italic = italic;
		this.underlined = underlined;
		this.strikethrough = strikethrough;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Runnable getAction() {
		return action;
	}

	public void setAction(Runnable action) {
		this.action = action;
	}

	public ChatColor getColor() {
		return color;
	}

	public void setColor(ChatColor color) {
		this.color = color;
	}

	public boolean isBold() {
		return bold;
	}

	public void setBold(boolean bold) {
		this.bold = bold;
	}

	public boolean isItalic() {
		return italic;
	}

	public void setItalic(boolean italic) {
		this.italic = italic;
	}

	public boolean isUnderlined() {
		return underlined;
	}

	public void setUnderlined(boolean underlined) {
		this.underlined = underlined;
	}

	public boolean isStrikethrough() {
		return strikethrough;
	}

	public void setStrikethrough(boolean strikethrough) {
		this.strikethrough = strikethrough;
	}
	
}
