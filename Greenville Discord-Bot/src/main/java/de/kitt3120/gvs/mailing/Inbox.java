package de.kitt3120.gvs.mailing;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.logging.Logger;
import de.kitt3120.gvs.managers.files.FileManager;
import de.kitt3120.gvs.modules.Module;
import de.kitt3120.gvs.users.User;

public class Inbox {
	
	private User owner;
	private ArrayList<Mail> mails = new ArrayList<>();
	
	public Inbox(User owner) {
		this.owner = owner;
	}
	
	@SuppressWarnings("unchecked")
	public void loadMails() {
		Logger.startSection("Inbox");
		Logger.log("Loading mails of user: " + owner.getUserId().toString(), false);
		mails.clear();
		
		File mailFile = FileManager.getFile("UserData/" + owner.getUserId().toString() + "/mails.gv");
		if(mailFile.exists()) {
			Logger.log("Found file: " + mailFile.getPath(), false);
			try {
				ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(mailFile)));
				mails = (ArrayList<Mail>) ois.readObject();
				ois.close();
				Logger.log("Loaded file: " + mailFile.getPath(), false);
			} catch (IOException e) {
				e.printStackTrace();
				Logger.severe("Could not load user's mail file: " + mailFile.getPath());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				Logger.severe("Could not find class while reading mails file: " + mailFile.getPath());
			}
		} else {
			Logger.log("No file found! Creating empty mail list.", false);
			mails = new ArrayList<>();
		}
		
		Logger.endSection();
	}
	
	public void saveMails() {
		Logger.startSection("Inbox");
		Logger.log("Saving mails of user: " + owner.getUserId().toString(), false);
		
		File mailFile = FileManager.getFile("UserData/" + owner.getUserId().toString() + "/mails.gv");
		try {
			ObjectOutput ous = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(mailFile)));
			ous.writeObject(mails);
			ous.close();
		} catch (IOException e) {
			e.printStackTrace();
			Logger.severe("Could not save user's mail file");
		}
		
		Logger.endSection();
	}
	
	public User getOwner() {
		return owner;
	}
	
	public ArrayList<Mail> getMails() {
		return mails;
	}

	public boolean isEmpty() {
		return mails.size() == 0;
	}
	
	public void addMail(Mail mail) {
		if(!mails.contains(mail)) mails.add(mail);
	}

	public void removeMail(Mail mail) {
		if(mails.contains(mail)) mails.remove(mail);
	}

	public void send(Mail mail) {
		Inbox receiverInbox = Core.getInstance().getInboxManager().getInbox(mail.getReceiver());
		if(receiverInbox == null) {
			Logger.startSection("Inbox Mail Sending");
			Logger.severe("Failed to send mail: Inbox is null");
			Logger.endSection();
			mail.getSender().toMinecraftPlayer().sendMessage(Module.getPrefix() + " Failed to send mail to " + mail.getReceiver().toMinecraftPlayerOffline().getName());
		} else {
			receiverInbox.addMail(mail);
		}
	}
	
}
