package de.kitt3120.gvs.mailing;

import java.util.ArrayList;

import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.logging.Logger;
import de.kitt3120.gvs.users.User;

public class InboxManager {
	
	private ArrayList<Inbox> inboxes;
	
	private BukkitTask autoSaver;
	
	public InboxManager() {
		Logger.startSection("Inbox-Manager");
		
		Logger.log("Initializing Inbox-Manager", false);
		inboxes = new ArrayList<>();
		
		Logger.log("Loading all user's mails");
		loadMails();
		
		Logger.log("Starting Auto-Saver", false);
		startAutoSaver();
		
		Logger.endSection();
	}

	public void loadMails() {
		Logger.startSection("Inbox-Manager");
		Logger.log("Loading mails", false);
		inboxes.clear();
		
		for (User user : Core.getInstance().getUserManager().getUsers()) {
			Inbox inbox = new Inbox(user);
			inbox.loadMails();
			inboxes.add(inbox);
		}
		Logger.endSection();
	}
	
	public void saveMails() {
		Logger.startSection("Inbox-Manager");
		Logger.log("Saving mails", false);
		for(Inbox inbox : inboxes) {
			inbox.saveMails();
		}
		Logger.endSection();
	}
	
	public void startAutoSaver() {
		if(autoSaver == null) {
			Logger.startSection("Inbox-Manager Auto-Saver");
			Logger.log("Starting Auto-Saver", false);
			autoSaver = new BukkitRunnable() {
				
				@Override
				public void run() {
					saveMails();
				}
			}.runTaskTimerAsynchronously(Core.getInstance(), 20 * 60L, 20 * 60L);
			Logger.endSection();
		}
	}

	public void stopAutoSaver() {
		if(autoSaver != null) {
			Logger.startSection("Inbox-Manager Auto-Saver");
			Logger.log("Stopping Auto-Saver", false);
			autoSaver.cancel();
			autoSaver = null;
			Logger.endSection();
		}
	}
	
	public Inbox getInbox(User user) {
		for (Inbox inbox : inboxes) {
			if(inbox.getOwner().getUserId().toString().equals(user.getUserId().toString())) return inbox;
		}
		
		Inbox inbox = new Inbox(user);
		inboxes.add(inbox);
		inbox.saveMails();
		return inbox;
	}

}
