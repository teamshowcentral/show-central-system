package de.kitt3120.gvs.mailing;

import java.util.ArrayList;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.users.User;

public class MassMailer {

	public static void send(User user, String message, ArrayList<User> users) {
		send(Core.getInstance().getInboxManager().getInbox(user), message, users);
	}

	public static void send(Inbox inbox, String message, ArrayList<User> users) {
		for(User user : users) {
			inbox.send(new Mail(inbox.getOwner(), user, message));
		}
	}

}
