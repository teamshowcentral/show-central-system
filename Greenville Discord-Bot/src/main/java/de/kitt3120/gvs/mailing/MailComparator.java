package de.kitt3120.gvs.mailing;

import java.util.Comparator;

public class MailComparator implements Comparator<Mail> {

	@Override
	public int compare(Mail m1, Mail m2) {
		if((m1.hasRead() && m2.hasRead()) || (!m1.hasRead() && !m2.hasRead())) {
			if(m1.getDate().before(m2.getDate())) return -1;
			else return 1;
		} else {
			if(m1.hasRead() && !m2.hasRead()) return -1;
			else return 1;
		}
	}

}
