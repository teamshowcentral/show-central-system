package de.kitt3120.gvs.mailing;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import de.kitt3120.gvs.users.User;

public class Mail implements Serializable {
	
	private static final long serialVersionUID = -3837930931803395964L;
	
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	
	private UUID uuid;
	private User sender;
	private User receiver;
	private String message;
	private boolean hasRead;
	private Date date;
	
	public Mail(User sender, User receiver, String message) {
		this.date = Calendar.getInstance().getTime();
		
		this.uuid = UUID.randomUUID();
		this.sender = sender;
		this.receiver = receiver;
		this.message = message;
		this.hasRead = false;
	}

	public User getSender() {
		return sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public String getMessage() {
		return message;
	}

	public Date getDate() {
		return date;
	}
	
	public String getDateAsString() {
		return dateFormat.format(date);
	}
	
	public boolean hasRead() {
		return hasRead;
	}
	
	public void setRead(boolean read) {
		hasRead = read;
	}

	public UUID getUUID() {
		return uuid;
	}

}
