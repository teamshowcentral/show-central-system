package de.kitt3120.gvs.managers.files;

import java.io.File;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.logging.Logger;

public class FileManager {
	
	private static boolean isSetUp = false;
	private static File root;
	
	private static void setup() {
		root = Core.getInstance().getDataFolder();
		isSetUp = true;
	}

	public static File getFile(String name) {
		if(!isSetUp) setup();
		
		File toReturn = new File(root, name);
		if(toReturn.getParentFile().mkdirs()) {
			Logger.startSection("File-Manager");
			Logger.log("Created file " + toReturn.getParentFile().getAbsolutePath() + " to access file " + name, false);
			Logger.endSection();
		}
		return toReturn;
	}

	public static File getFile(File dir, String name) {
		if(!isSetUp) setup();
		
		File toReturn = new File(dir, name);
		if(toReturn.getParentFile().mkdirs()) {
			Logger.startSection("File-Manager");
			Logger.log("Created file " + toReturn.getParentFile().getAbsolutePath() + " to access file " + name, false);
			Logger.endSection();
		}
		return toReturn;
	}
	
}
