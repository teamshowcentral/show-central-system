package de.kitt3120.gvs.modules.modules.mailing;

import java.util.ArrayList;
import java.util.Collections;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.interaction.discord.polls.Poll;
import de.kitt3120.gvs.interaction.discord.polls.PollListener;
import de.kitt3120.gvs.interaction.minecraft.choicemessages.Choice;
import de.kitt3120.gvs.interaction.minecraft.choicemessages.ChoiceMessage;
import de.kitt3120.gvs.mailing.Inbox;
import de.kitt3120.gvs.mailing.MailComparator;
import de.kitt3120.gvs.misc.Permissions;
import de.kitt3120.gvs.modules.Module;
import de.kitt3120.gvs.modules.ModuleCategory;
import de.kitt3120.gvs.users.User;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveEvent;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;

public class Mails extends Module {

	public Mails() {
		super("Mails", "Opens your inbox", ModuleCategory.Utility, false);
	}

	@Override
	public boolean isAllowed(User user) {
		return Permissions.isAdmin(user) || user.toMinecraftPlayer().hasPermission(Permissions.Mailing);
	}

	@Override
	public void onFireFromMinecraft(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(getPrefix() + " Sorry console buddy, you don't have an inbox :(");
			return;
		}
		Player player = (Player) sender;
		User user = Core.getInstance().getUserManager().getUser(player);
		Inbox inbox = Core.getInstance().getInboxManager().getInbox(user);
		if(inbox.isEmpty()) {
			player.sendMessage(getPrefix() + " Your inbox is empty");
		} else {
			ArrayList<de.kitt3120.gvs.mailing.Mail> mails = inbox.getMails();
			mails.sort(new MailComparator());
			Collections.reverse(mails);
			
			for (int i = mails.size() - 1; i >= 0; i--) {
				de.kitt3120.gvs.mailing.Mail mail = mails.get(i);
				ChoiceMessage mailMessage = new ChoiceMessage(mail.getDateAsString() + " FROM " + mail.getSender().toMinecraftPlayerOffline().getName(), 30);
				mailMessage.setKillOnChoice(false);
				if(mail.hasRead()) mailMessage.setMainColor(ChatColor.DARK_GRAY);
				else mailMessage.setMainColor(ChatColor.GRAY);
				mailMessage.addChoice(new Choice("Show", ChatColor.DARK_AQUA, new Runnable() {
					
					@Override
					public void run() {
						TextComponent msg = new TextComponent();
						msg.setColor(ChatColor.DARK_GRAY);
						for(int i = 0; i < 7; i++) msg.addExtra("-");
						msg.addExtra("\n");
						
						TextComponent header = new TextComponent();
						header.setColor(ChatColor.GRAY);
						header.addExtra("From " + mail.getSender().toMinecraftPlayerOffline().getName() + " on " + mail.getDateAsString());
						header.addExtra("\n");
						header.addExtra("\n");
						msg.addExtra(header);
						
						TextComponent body = new TextComponent();
						body.setColor(ChatColor.GRAY);
						body.setItalic(true);
						body.addExtra(mail.getMessage());
						body.addExtra("\n");
						msg.addExtra(body);
						
						for(int i = 0; i < 7; i++) msg.addExtra("-");
						player.spigot().sendMessage(msg);
						
						if(!mail.hasRead()) mail.setRead(true);
					}
				}));
				mailMessage.addChoice(new Choice(mail.hasRead() ? "Mark as unread" : "Mark as read", mail.hasRead() ? ChatColor.DARK_RED: ChatColor.GREEN, new Runnable() {
					
					@Override
					public void run() {
						mail.setRead(!mail.hasRead());
						if(mail.hasRead()) {
							player.sendMessage(getPrefix() + " §7Marked as read");
						} else {
							player.sendMessage(getPrefix() + " §7Marked as unread");
						}
					}
				}));
				mailMessage.addChoice(new Choice("Delete", ChatColor.RED, new Runnable() {
					
					@Override
					public void run() {
						inbox.removeMail(mail);
						player.sendMessage(getPrefix() + " §7Mail deleted");
						mailMessage.kill();
					}
				}));
				
				mailMessage.send(player);
			}
		}
	}

	@Override
	public void onFireFromDiscord(net.dv8tion.jda.core.entities.User user, MessageChannel channel, String[] args) {
		if(!Core.getInstance().getUserManager().existsUser(user)) {
			channel.sendMessage("Sorry, " + user.getAsMention() + "! You do not have your Minecraft Account linked :frowning:").queue();
			return;
		}
		
		User usr = Core.getInstance().getUserManager().getUser(user);
		Inbox inbox = Core.getInstance().getInboxManager().getInbox(usr);
		
		if(inbox.isEmpty()) {
			channel.sendMessage("Your inbox is empty!").queue();
			return;
		}
		
		for(de.kitt3120.gvs.mailing.Mail mail : inbox.getMails()) {
			String readEmoji = mail.hasRead() ? "x" : "white_check_mark";
			String readText = mail.hasRead() ? "Mark as unread" : "Mark as read";
			String[] options = new String[] { "newspaper", readEmoji, "red_circle" };
			
			Poll mailPoll = new Poll(usr, channel, "From " + mail.getSender().toMinecraftPlayerOffline().getName() + " on " + mail.getDateAsString() + "\n:" + options[0] + ": Show, :" + options[1] + ": " + readText + ", :" + options[2] + ": Delete", inbox.getMails().size() * 4, false) {
				@Override
				public boolean isFinished() {
					return getVotesCount() > 0 || getTimeout() <= 0;
				}
			};
			
			mailPoll.setOptions(options);
			mailPoll.addPollListener(new PollListener() {
				
				@Override
				public void onTick(Poll poll) {}
				
				@Override
				public void onStop(Poll poll) {
					if(poll.getVotes(0) > 0) {
						mailPoll.getMessage().editMessage("From " + mail.getSender().toMinecraftPlayerOffline().getName() + " on " + mail.getDateAsString() + "\n```" + mail.getMessage() + "```").queue();
						if(!mail.hasRead()) mail.setRead(true);
					} else if(poll.getVotes(1) > 0) {
						mail.setRead(!mail.hasRead());
						mailPoll.getMessage().editMessage("Mail marked as " + (mail.hasRead() ? "read" : "unread")).queue();
					} else if(poll.getVotes(2) > 0) {
						inbox.removeMail(mail);
						mailPoll.getMessage().editMessage("Mail deleted!").queue();
					}
				}
				
				@Override
				public void onStart(Poll poll) {}
				
				@Override
				public void onReactionRemoved(Poll poll, MessageReactionRemoveEvent event) {}
				
				@Override
				public void onReactionAdded(Poll poll, MessageReactionAddEvent event) {}
			});
			
			mailPoll.setRefreshRate((long) ((20*inbox.getMails().size())*1.5));
			mailPoll.start();
		}
	}

	@Override
	public boolean isDiscordNeeded() {
		return true;
	}

	@Override
	public Module getInstance() {
		return this;
	}

}
