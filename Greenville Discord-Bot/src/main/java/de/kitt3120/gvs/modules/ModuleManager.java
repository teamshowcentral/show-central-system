package de.kitt3120.gvs.modules;

import java.io.File;
import java.util.ArrayList;

import de.kitt3120.gvs.logging.Logger;
import de.kitt3120.gvs.managers.files.FileManager;
import de.kitt3120.gvs.modules.modules.mailing.Mail;
import de.kitt3120.gvs.modules.modules.mailing.Mails;
import de.kitt3120.gvs.modules.modules.system.ChoiceMessageHandler;
import de.kitt3120.gvs.modules.modules.utility.Link;
import de.kitt3120.gvs.modules.modules.utility.Unlink;

public class ModuleManager {

	private ArrayList<Module> modules = new ArrayList<>();

	public ModuleManager() {
		Logger.startSection("Module-Manager");
		
		Logger.log("Initializing Module-Manager...", false);
		File modulesFolder = FileManager.getFile("ModuleData");
		if(modulesFolder.mkdirs()) Logger.log("Created Module-Data directory", false);
		
		initializeModules();
		
		Logger.endSection();
	}

	public void initializeModules() {
		Logger.startSection("Module-Manager");
		Logger.log("Initializing Modules");
		//TODO: Register all modules!

		register(new ChoiceMessageHandler());
		register(new Link());
		register(new Unlink());
		register(new Mails());
		register(new Mail());
		
		Logger.endSection();
	}
	
	public void register(Module module) {
		if(!modules.contains(module)) {
			Logger.startSection("Module-Manager");
			Logger.log("Enabling Module " + module.getName());
			modules.add(module);
			module.loadStorage();
			module.onEnable();
			Logger.endSection();
		}
	}
	
	public void unregister(Module module) {
		if(modules.contains(module)) {
			Logger.startSection("Module-Manager");
			Logger.log("Disabling Module " + module.getName());
			modules.remove(module);
			module.saveStorage();
			module.onDisable();
			Logger.endSection();
		}
	}

	public void unregisterAll() {
		Logger.startSection("Module-Manager");
		Logger.log("Unregistering all Modules", false);
		while (modules.size() > 0) {
			unregister(modules.get(0));
		}
		Logger.endSection();
	}
	
	public void saveAll() {
		Logger.startSection("Module-Manager");
		Logger.log("Saving all module's storages...");
		for (Module module : modules) {
			module.saveStorage();
		}
		Logger.endSection();
	}
	
	public Module getModule(String name) { 
		return getModule(name, false, true);
	}
	
	public Module getModule(String name, boolean passives, boolean ignoreCase) {
		for (Module module : modules) {
			if(module.isPassive() && !passives) continue;
			if(ignoreCase ? module.getName().toLowerCase().equalsIgnoreCase(name) : module.getName().equals(name)) return module;
		}
		return null;
	}

}
