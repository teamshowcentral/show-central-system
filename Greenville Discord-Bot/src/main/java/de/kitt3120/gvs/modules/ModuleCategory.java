package de.kitt3120.gvs.modules;

public enum ModuleCategory {
	Administration, Utility, Fun, System, Misc
}
