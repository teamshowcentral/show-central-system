package de.kitt3120.gvs.modules.modules.utility;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.botcore.Bot;
import de.kitt3120.gvs.interaction.discord.polls.Poll;
import de.kitt3120.gvs.interaction.discord.polls.PollListener;
import de.kitt3120.gvs.interaction.minecraft.choicemessages.Choice;
import de.kitt3120.gvs.interaction.minecraft.choicemessages.ChoiceMessage;
import de.kitt3120.gvs.interaction.minecraft.choicemessages.ChoiceMessageListener;
import de.kitt3120.gvs.misc.Permissions;
import de.kitt3120.gvs.modules.Module;
import de.kitt3120.gvs.modules.ModuleCategory;
import de.kitt3120.gvs.users.User;
import de.kitt3120.gvs.users.UserProperty;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveEvent;
import net.md_5.bungee.api.ChatColor;

public class Link extends Module {

	public Link() {
		super("Link", "Link your Minecraft Account with your Discord Account", ModuleCategory.Utility, false);
	}

	@Override
	public boolean isAllowed(User user) {
		return Permissions.isAdmin(user) || user.toMinecraftPlayer().hasPermission(Permissions.Module_Link);
	}

	@Override
	public void onFireFromMinecraft(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(getPrefix() + " This module can't be fired from console!");
			return;
		} else if(args.length < 1) {
			sender.sendMessage(getPrefix() + " Please also provide your Discord ID and use the command like this:\n/" + getName() + " ID\nIf you don't know how to see your Discord ID:\n-Find the bot on Discord\n-Open a private chat with it\n-Type $Link");
			return;
		}
		
		Player player = (Player) sender;
		
		if(Core.getInstance().getUserManager().getUser(player).isDiscordBound()) {
			player.sendMessage(getPrefix() + " Can't link Discord-Account: Already linked, use Unlink command!");
			return;
		}
		
		String discordId = args[0];
		net.dv8tion.jda.core.entities.User user = Bot.getBot().getUserById(discordId);
		
		if(user == null) {
			player.sendMessage(getPrefix() + " Can't find a user with that ID. Maybe you did not join the Discord server yet?");
			return;
		}
		
		ChoiceMessage correctUserVerify = new ChoiceMessage(getPrefix() + "\nFound user: " + user.getName() + "#" + user.getDiscriminator(), 30);
		correctUserVerify.addChoice(new Choice("That's me!", ChatColor.GREEN, new Runnable() {
			public void run() {
				player.sendMessage(getPrefix() + " Please open your Discord!");
				
				Poll confirmDiscord = new Poll(Core.getInstance().getUserManager().getUser(player), user.openPrivateChannel().complete(), "Link with Minecraft-Account: " + player.getName() + "?", 30) {
					@Override
					public boolean isFinished() {
						return getVotesCount() > 0 || getTimeout() <= 0;
					}
				};
				
				confirmDiscord.addPollListener(new PollListener() {
					
					@Override
					public void onTick(Poll poll) {}
					
					@Override
					public void onStop(Poll poll) {
						poll.getMessage().delete().queue();
						
						if(poll.isFinished() && poll.getVotes(0) > 0) {
							User u = Core.getInstance().getUserManager().getUser(player);
							u.setProperty(UserProperty.UUID_Discord, user.getId());
							u.saveProperties();
							player.sendMessage(getPrefix() + " Successfully linked Discord-Account!");
							user.openPrivateChannel().complete().sendMessage("Successfully linked Minecraft-Account!").queue();
						} else {
							player.sendMessage(getPrefix() + " Failed to link Discord-Account");
							user.openPrivateChannel().complete().sendMessage("Failed to link Minecraft-Account!").queue();
						}
					}
					
					@Override
					public void onStart(Poll poll) {}
					
					@Override
					public void onReactionRemoved(Poll poll, MessageReactionRemoveEvent event) {}
					
					@Override
					public void onReactionAdded(Poll poll, MessageReactionAddEvent event) {}
				});
			}
		}, true));
		correctUserVerify.addChoice(new Choice("Nope!", ChatColor.RED, new Runnable() {
			
			@Override
			public void run() {
				player.sendMessage(getPrefix() + " Alright, cancelled it!");
			}
		}, true));
		correctUserVerify.addChoiceMessageListener(new ChoiceMessageListener() {
			
			@Override
			public void onRegister(ChoiceMessage choiceMessage) {}
			
			@Override
			public void onKill(ChoiceMessage choiceMessage) {}
			
			@Override
			public void onTimeout(ChoiceMessage choiceMessage) {
				player.sendMessage(getPrefix() + " No answer was given. Cancelled Discord linking.");
			}
			
			@Override
			public void onChoice(ChoiceMessage choiceMessage, Choice choice, int choiceId) {}
		});
		correctUserVerify.send(player);
	}

	@Override
	public void onFireFromDiscord(net.dv8tion.jda.core.entities.User user, MessageChannel channel, String[] args) {
		channel.sendMessage(user.getAsMention() + " run this command ingame: /" + getName() + " " + user.getId()).queue();
	}

	@Override
	public boolean isDiscordNeeded() {
		return false;
	}

	@Override
	public Module getInstance() {
		return this;
	}

}
