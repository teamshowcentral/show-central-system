package de.kitt3120.gvs.modules.modules.utility;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.misc.Permissions;
import de.kitt3120.gvs.modules.Module;
import de.kitt3120.gvs.modules.ModuleCategory;
import de.kitt3120.gvs.users.User;
import de.kitt3120.gvs.users.UserProperty;
import net.dv8tion.jda.core.entities.MessageChannel;

public class Unlink extends Module {

	public Unlink() {
		super("Unlink", "Unlinks a Discord-Account", ModuleCategory.Utility, false);
	}

	@Override
	public boolean isAllowed(User user) {
		return Permissions.isAdmin(user) || user.toMinecraftPlayer().hasPermission(Permissions.Module_Link);
	}

	@Override
	public void onFireFromMinecraft(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(getPrefix() + " This module can't be fired from console!");
		} else {
			User user = Core.getInstance().getUserManager().getUser(((Player) sender));
			if(user.isDiscordBound()) {
				user.removeProperty(UserProperty.UUID_Discord);
				sender.sendMessage(getPrefix() + " Successfully unlinked Discord-Account!");
			} else {
				sender.sendMessage(getPrefix() + " You do not have a Discord-Account linked!");
			}
		}
	}

	@Override
	public void onFireFromDiscord(net.dv8tion.jda.core.entities.User user, MessageChannel channel, String[] args) {
		if(!Core.getInstance().getUserManager().existsUser(user)) {
			user.openPrivateChannel().complete().sendMessage("You do not have a Discord-Account linked!").queue();
		} else {
			Core.getInstance().getUserManager().getUser(user).removeProperty(UserProperty.UUID_Discord);
			user.openPrivateChannel().complete().sendMessage("Successfully unlinked Discord-Account!").queue();
		}
	}

	@Override
	public boolean isDiscordNeeded() {
		return false;
	}

	@Override
	public Module getInstance() {
		return this;
	}

}
