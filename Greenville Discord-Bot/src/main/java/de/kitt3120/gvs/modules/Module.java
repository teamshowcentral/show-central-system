package de.kitt3120.gvs.modules;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.botcore.Bot;
import de.kitt3120.gvs.logging.Logger;
import de.kitt3120.gvs.managers.files.FileManager;
import de.kitt3120.gvs.users.User;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public abstract class Module extends ListenerAdapter implements Listener {
	
	public static String getPrefix() {
		return "§a[§2GV§a]§7";
	}
	
	private String name, description;
	private ModuleCategory moduleCategory;
	private boolean isPassive;
	
	private HashMap<String, Object> moduleStorage;
	
	public Module(String name, String description, ModuleCategory moduleCategory, boolean isPassive) {
		this.name = name;
		this.description = description;
		this.moduleCategory = moduleCategory;
		this.isPassive = isPassive;
		
		this.moduleStorage = new HashMap<>();
	}

	public abstract boolean isAllowed(User user);
	
	public boolean isAllowed(Player player) {
		return isAllowed(Core.getInstance().getUserManager().getUser(player));
	}
	
	public boolean isAllowed(CommandSender sender) {
		if(sender instanceof Player)
			return isAllowed((Player) sender);
		else if(sender instanceof ConsoleCommandSender)
			return true;
		else return false;
	}
	
	public abstract void onFireFromMinecraft(CommandSender sender, String[] args);
	public abstract void onFireFromDiscord(net.dv8tion.jda.core.entities.User user, MessageChannel channel, String[] args);
	public void onFireUniversal(User user, FireSource fireSource) {};
	
	public void onEnable() {
		Logger.startSection("Module " + name);
		Logger.log("Registering listeners", false);
		Core.getInstance().getServer().getPluginManager().registerEvents(getInstance(), Core.getInstance());
		Bot.getBot().addEventListener(getInstance());
		Logger.endSection();
	};
	
	public void onDisable() {
		Logger.startSection("Module " + name);
		Logger.log("Unregistering listeners", false);
		HandlerList.unregisterAll(this);
		Bot.getBot().removeEventListener(getInstance());
		Logger.endSection();
	};
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public ModuleCategory getModuleCategory() {
		return moduleCategory;
	}
	
	public boolean isPassive() {
		return isPassive;
	}
	
	public boolean hasInStorage(String key) {
		return moduleStorage.containsKey(key);
	}
	
	public Object getFromStorage(String key) {
		return moduleStorage.get(key);
	}
	
	public void putToStorage(String key, Object value) {
		moduleStorage.put(key, value);
	}
	
	public void removeFromStorage(String key) {
		moduleStorage.remove(key);
	}
	
	@SuppressWarnings("unchecked")
	public void loadStorage() {
		Logger.startSection("Module " + name);
		Logger.log("Loading storage...", false);
		if(moduleStorage != null) moduleStorage.clear();
		
		File storageFile = FileManager.getFile(getModuleFolder(), "Storage.strg");
		if(storageFile.exists()) {
			Logger.log("Found storage file", false);
			try {
				ObjectInput in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(storageFile)));
				moduleStorage = (HashMap<String, Object>) in.readObject();
				in.close();
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
				Logger.severe("Could not load storage!");
			}
		} else {
			Logger.log("No storage file found, creating empty storage");
			moduleStorage = new HashMap<>();
		}
		Logger.endSection();
	}
	
	public void saveStorage() {
		Logger.startSection("Module " + name);
		Logger.log("Saving storage...", false);
		try {
			ObjectOutput out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(FileManager.getFile(getModuleFolder(), "Storage.strg"))));
			out.writeObject(moduleStorage);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			Logger.severe("Could not save storage!");
		}
		Logger.endSection();
	}
	
	public File getModuleFolder() {
		File moduleFolder = FileManager.getFile("ModuleData/" + name);
		if(!moduleFolder.exists()) moduleFolder.mkdirs();
		return moduleFolder;
	}
	
	public abstract boolean isDiscordNeeded();
	public abstract Module getInstance();
}
