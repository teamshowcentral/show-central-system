package de.kitt3120.gvs.modules.modules.mailing;

import java.util.ArrayList;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.kitt3120.gvs.Core;
import de.kitt3120.gvs.interaction.minecraft.choicemessages.Choice;
import de.kitt3120.gvs.interaction.minecraft.choicemessages.ChoiceMessage;
import de.kitt3120.gvs.mailing.MassMailer;
import de.kitt3120.gvs.misc.Permissions;
import de.kitt3120.gvs.modules.Module;
import de.kitt3120.gvs.modules.ModuleCategory;
import de.kitt3120.gvs.users.User;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.md_5.bungee.api.ChatColor;

public class Mail extends Module {

	public Mail() {
		super("Mail", "Send a mail", ModuleCategory.Utility, false);
	}

	@Override
	public boolean isAllowed(User user) {
		return Permissions.isAdmin(user) || user.toMinecraftPlayer().hasPermission(Permissions.Mailing);
	}

	@Override
	public void onFireFromMinecraft(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(getPrefix() + " Sorry console buddy, you don't have the ability to send mails :(");
			return;
		}
		Player player = (Player) sender;
		
		if(args.length < 3) {
			player.sendMessage(getPrefix() + " Syntax: /mail msg @ user1 user2 user3 ...");
			return;
		}
		
		boolean messageValid = false;
		for(String arg : args) if(arg.equals("@")) messageValid = true;
		
		if(!messageValid) {
			player.sendMessage(getPrefix() + " Syntax: /mail msg @ user1 user2 user3 ...");
			return;
		}

		StringBuilder messageBuilder = new StringBuilder();
		StringBuilder usernamesBuilder = new StringBuilder();
		boolean builderSwitch = false;
		
		for(String arg : args) {
			if(arg.equals("@")) {
				builderSwitch = true; 
				continue;
			}
			
			if(!builderSwitch) messageBuilder.append(arg + " ");
			else usernamesBuilder.append(arg + " ");
		}
		
		if(messageBuilder.toString().trim().length() == 0 || usernamesBuilder.toString().trim().length() == 0) messageValid = false; 
		
		if(!messageValid) {
			player.sendMessage(getPrefix() + " Syntax: /mail msg @ user1 user2 user3 ...");
			return;
		}
		
		String message = messageBuilder.toString().trim();
		ArrayList<User> users = new ArrayList<>();
		
		for(String userName : usernamesBuilder.toString().trim().split(" ")) {
			User u = Core.getInstance().getUserManager().getUser(userName);
			if(u == null) {
				player.sendMessage(getPrefix() + " Could not find user: " + userName);
				continue;
			}
			
			if(!users.contains(u)) users.add(u);
		}
		
		StringBuilder validUsernamesBuilder = new StringBuilder();
		for(User user : users) {
			validUsernamesBuilder.append(user.toMinecraftPlayerOffline().getName() + " ");
		}
		
		if(users.isEmpty()) {
			player.sendMessage(getPrefix() + " No users given");
			return;
		}
		
		ChoiceMessage sendConfirm = new ChoiceMessage("Do you want to send the mail to the following users?\n" + validUsernamesBuilder.toString().trim().replace(" ", ", "), 30);
		sendConfirm.addChoice(new Choice("Send", ChatColor.GREEN, new Runnable() {
			
			@Override
			public void run() {
				MassMailer.send(Core.getInstance().getUserManager().getUser(player), message, users);
				player.sendMessage(getPrefix() + " Sent!");
			}
		}));
		
		sendConfirm.addChoice(new Choice("Cancel", ChatColor.RED, new Runnable() {
	
			@Override
			public void run() {
				player.sendMessage(getPrefix() + " Cancelled!");
			}
		}));
		
		sendConfirm.send(player);
		
	}

	@Override
	public void onFireFromDiscord(net.dv8tion.jda.core.entities.User user, MessageChannel channel, String[] args) {
		channel.sendMessage("This module's Discord support is under construction :wink:").queue();
	}

	@Override
	public boolean isDiscordNeeded() {
		return true;
	}

	@Override
	public Module getInstance() {
		return this;
	}

}
