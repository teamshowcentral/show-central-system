package de.kitt3120.gvs.modules.modules.system;

import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.kitt3120.gvs.interaction.minecraft.choicemessages.ChoiceMessageRegistry;
import de.kitt3120.gvs.modules.Module;
import de.kitt3120.gvs.modules.ModuleCategory;
import de.kitt3120.gvs.users.User;
import net.dv8tion.jda.core.entities.MessageChannel;

public class ChoiceMessageHandler extends Module {

	public ChoiceMessageHandler() {
		super("ChoiceMessageHandler", "This module reacts when a player clicks on a choice", ModuleCategory.System, true);
	}

	@Override
	public boolean isAllowed(User user) {
		return true;
	}

	@Override
	public void onFireFromMinecraft(CommandSender sender, String[] args) {
		try {
			if(sender instanceof Player) {
				Player player = (Player) sender;
				UUID uuid = UUID.fromString(args[0]);
				if(ChoiceMessageRegistry.existsMessage(uuid)) {
					int choiceId = Integer.parseInt(args[1]);
					ChoiceMessageRegistry.getChoiceMessage(uuid).choice(player, choiceId);
				}
			} else {
				sender.sendMessage(Module.getPrefix() + " Come on, you can't do that from console, you silly... :p");
			}
		} catch (Exception e) {
			sender.sendMessage(Module.getPrefix() + " This command is not meant to be run manually :) But good job, you found a hidden command!");
		}
	}

	@Override
	public void onFireFromDiscord(net.dv8tion.jda.core.entities.User user, MessageChannel channel, String[] args) {
		channel.sendMessage("This command is not meant to be run from Discord :wink: But good job, you found a hidden command!").queue();
	}

	@Override
	public boolean isDiscordNeeded() {
		return false;
	}

	@Override
	public Module getInstance() {
		return this;
	}

}
