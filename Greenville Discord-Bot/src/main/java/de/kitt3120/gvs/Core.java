package de.kitt3120.gvs;

import org.bukkit.plugin.java.JavaPlugin;

import de.kitt3120.gvs.botcore.Bot;
import de.kitt3120.gvs.botcore.InputHandler;
import de.kitt3120.gvs.logging.Logger;
import de.kitt3120.gvs.mailing.InboxManager;
import de.kitt3120.gvs.modules.ModuleManager;
import de.kitt3120.gvs.users.UserManager;

public class Core extends JavaPlugin {
	
	private static Core instance;
	
	private UserManager userManager;
	private InboxManager inboxManager;
	private ModuleManager moduleManager;
	private InputHandler inputHandler;
	
	//TODO: Mail module
	
	@Override
	public void onEnable() {
		instance = this;
		Logger.log("Created instance", false);
		
		Logger.log("Setting up JDA");
		Bot.setup();
		
		Logger.log("Setting up User-Manager");
		userManager = new UserManager();
		
		Logger.log("Setting up Inbox-Manager");
		inboxManager = new InboxManager();
			
		Logger.log("Setting up Module-Manager");
		moduleManager = new ModuleManager();
		
		Logger.log("Hooking Minecraft and Discord inputs");
		inputHandler = new InputHandler();
		
		Logger.log("Finished!");
	}
	
	@Override
	public void onDisable() {
		Logger.log("Shutting down...");
		
		inboxManager.saveMails();
		inboxManager.stopAutoSaver();
		
		userManager.saveUsers();
		userManager.stopAutoSaver();
		
		moduleManager.unregisterAll();
		inputHandler.unregister();
		
		Bot.shutdown();
		
		Logger.log("Bye! :)");
	}
	
	public UserManager getUserManager() {
		return userManager;
	}
	
	public InboxManager getInboxManager() {
		return inboxManager;
	}
	
	public ModuleManager getModuleManager() {
		return moduleManager;
	}
	
	public InputHandler getInputHandler() {
		return inputHandler;
	}
	
	public static Core getInstance() {
		return instance;
	}

}
